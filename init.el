(require 'package)
(add-to-list 'package-archives
	     '("melpa" . "http://melpa.org/packages/") t)

(package-initialize)


(setq package-selected-packages
      '(
	elpy
	monokai-pro-theme
	nim-mode
	web-mode
	sr-speedbar
	))
(package-install-selected-packages)


(require 'web-mode)
(add-to-list 'auto-mode-alist '("\\.html?\\'" . web-mode))
(setq web-mode-engines-alist
      '(("django"    . "\\.html\\'"))
      )

;; web-mode brackets auto closing
(setq web-mode-enable-auto-closing t)
(setq web-mode-enable-auto-pairing t)

;; enable elpy
(elpy-enable)

;; Monokai theme
(load-theme 'monokai-pro t)

;; Mous support on
(xterm-mouse-mode 1)

;; hide the startup screen 
(setq inhibit-startup-message t)

;; enable line number globally
(global-linum-mode t)

;; Backup config
(setq backup-directory-alist '(("." . "~/.emacs.d/backup"))
  backup-by-copying t    ; Don't delink hardlinks
  version-control t      ; Use version numbers on backups
  delete-old-versions t  ; Automatically delete excess backups
  kept-new-versions 20   ; how many of the newest versions to keep
  kept-old-versions 5    ; and how many of the old
  )




;; fix meta-keys on mac 
(if (eq system-type 'darwin)
    (if (eq window-system 'nil)
	(progn                                        
	  (global-set-key "\M-l" '(lambda () (interactive) (insert "@")))
	  (global-set-key "\M-5" '(lambda () (interactive) (insert "[")))
	  (global-set-key "\M-6" '(lambda () (interactive) (insert "]")))
	  (global-set-key "\M-7" '(lambda () (interactive) (insert "|")))
	  (global-set-key "\M-/" '(lambda () (interactive) (insert "\\")))
	  (global-set-key "\M-8" '(lambda () (interactive) (insert "{")))
	  (global-set-key "\M-9" '(lambda () (interactive) (insert "}")))
	  (global-set-key "\M-n" '(lambda () (interactive) (insert "~")))
	  )))
